#include <iostream>
#include <cstring>

using namespace std;
//
// La Pokédex ayudaba a Ash Ketchum desde los inicios de su poke-aventura a reconocer los distintos pokémon
// que iba  encontrando en su camino. Inicialmente solo contenía 150 pokémon ya que estaba en sus primeras etapas.
//
//Los pokémon de la primera versión de la pokedex tenían el nombre, descripción, altura, peso,
// tipo (eléctrico, agua, roca, etc...)
//y su debilidad (fuego, hoja, etc...)
//
//Ud se encuentra haciendo un remake de dichos juegos en C++, para su primera versión es
// necesario crear las estructuras básicas.
//
//Se pide:
// - Cree la clase Pokemon
// - Cree los constructores de la clase Pokemon, uno con todos sus parámetros y otro vacío.
// - Cree los set y get de la clase Pokemon
// - Cree la clase Pokedex que permita almacenar 150 pokemons.
// - Cree el método agregarPokemon(*Pokemon, int) en la clase Pokedex, este método recibe un puntero hacia
// una Pokémon y el número (del 1 al 150) dentro de la Pokédex.
// - Cree el método *buscarPokemon(int) en la clase Pokedex, este método retorna un puntero de Pokémon si es que
// existe, si no, retorna null
// - Cree el método *buscarPokemon(string) en la clase Pokedex, este método retorna un puntero de Pokémon si hay
// algún Pokemon con el nombre entregado, si no, retorna null
// - Cree el método estadísticas() en la clase Pokedex, este método imprime la cantidad de Pokémon por tipo que
// hay en la Pokedex y la debilidad más frecuente.
//
//Consideraciones:
//Tipos de Pokémon posibles: Agua, Tierra, Roca, Eléctrico, Fuego y Hoja.
//

class pokemon {
private:
    string nombre;
    string descripcion;
    int altura;
    int peso;
    string tipo;
    string debilidad;
public:
    pokemon() {}

    pokemon(string nombre, string descripcion, int altura, int peso, string tipo, string debilidad) {
        this->nombre = nombre;
        this->descripcion = descripcion;
        this->altura = altura;
        this->peso = peso;
        this->tipo = tipo;
        this->debilidad = debilidad;
    }

    string getNombre() {
        return nombre;
    }

    string getDescripcion() {
        return descripcion;
    }

    string getTipo() {
        return tipo;
    }

    string getDebilidad() {
        return debilidad;
    }

    int getAltura() {
        return altura;
    }

    int getPeso() {
        return peso;
    }

    void setNombre(string nombre) {
        this->nombre = nombre;
    }

    void setDescripcion(string descripcion) {
        this->descripcion = descripcion;
    }

    void setTipo(string tipo) {
        this->tipo = tipo;
    }

    void setDebilidad(string debilidad) {
        this->debilidad = debilidad;
    }
};

class pokedex {
private:
    pokemon *lista[150];

public:
    pokedex() {
        for (int i = 0; i < 150; i++) {
            lista[i] = nullptr;
        }
    }

    void agregarPokemon(pokemon *nuevo, int numero) {
        lista[numero] = nuevo;
    }

    pokemon *buscarPokemon(int numero) {
        return lista[numero];
    }

    pokemon *buscarPokemon(string nombre) {
        for (int i = 0; i < 150; i++) {
            if (lista[i] && lista[i]->getNombre() == nombre) {
                return lista[i];
            }
        }
        return nullptr;
    }

    void estadisticas() {
        //Agua = 0, Tierra = 1, Roca = 2, Eléctrico = 3, Fuego = 4 y Hoja = 5
        int tipos[6] = {0};
        int debilidad[6] = {0};
        int debilidadMasFrecuenteCantidad = 0;
        string debilidadFrecuente = "Agua";
        for (int i = 0; i < 150; i++) {
            if (lista[i]) {
                if (lista[i]->getTipo() == "Agua") {
                    tipos[0]++;
                } else if (lista[i]->getTipo() == "Tierra") {
                    tipos[1]++;
                } else if (lista[i]->getTipo() == "Roca") {
                    tipos[2]++;
                } else if (lista[i]->getTipo() == "Electrico") {
                    tipos[3]++;
                } else if (lista[i]->getTipo() == "Fuego") {
                    tipos[4]++;
                } else if (lista[i]->getTipo() == "Hoja") {
                    tipos[5]++;
                }

                if (lista[i]->getDebilidad() == "Agua") {
                    debilidad[0]++;
                    if (debilidad[0] > debilidadMasFrecuenteCantidad) {
                        debilidadMasFrecuenteCantidad = debilidad[0];
                        debilidadFrecuente = "Agua";
                    }
                } else if (lista[i]->getDebilidad() == "Tierra") {
                    debilidad[1]++;
                    if (debilidad[1] > debilidadMasFrecuenteCantidad) {
                        debilidadFrecuente = "Tierra";
                        debilidadMasFrecuenteCantidad = debilidad[1];
                    }
                } else if (lista[i]->getDebilidad() == "Roca") {
                    debilidad[2]++;
                    if (debilidad[2] > debilidadMasFrecuenteCantidad) {
                        debilidadFrecuente = "Roca";
                        debilidadMasFrecuenteCantidad = debilidad[2];
                    }
                } else if (lista[i]->getDebilidad() == "Electrico") {
                    debilidad[3]++;
                    if (debilidad[3] > debilidadMasFrecuenteCantidad) {
                        debilidadFrecuente = "Electrico";
                        debilidadMasFrecuenteCantidad = debilidad[3];
                    }
                } else if (lista[i]->getDebilidad() == "Fuego") {
                    debilidad[4]++;
                    if (debilidad[4] > debilidadMasFrecuenteCantidad) {
                        debilidadFrecuente = "Fuego";
                        debilidadMasFrecuenteCantidad = debilidad[4];
                    }
                } else if (lista[i]->getDebilidad() == "Hoja") {
                    debilidad[5]++;
                    if (debilidad[5] > debilidadMasFrecuenteCantidad) {
                        debilidadFrecuente = "Hoja";
                        debilidadMasFrecuenteCantidad = debilidad[5];
                    }
                }
            }
        }
        cout << "Cantidad de pokémon por tipo:" << endl
        << "- Agua:\t\t\t" << tipos[0] << endl
             << "- Tierra:\t\t" << tipos[1] << endl
             << "- Roca:\t\t\t" << tipos[2] << endl
             << "- Electrico:\t" << tipos[3] << endl
             << "- Fuego:\t\t" << tipos[4] << endl
             << "- Hoja:\t\t\t" << tipos[5] << endl;

        cout << "Debilidad más frecuente:" << endl
             << "- " << debilidadFrecuente << ":\t" << debilidadMasFrecuenteCantidad << endl;
    }
};

int main() {
    pokedex *ash = new pokedex();

    pokemon *pikachu = new pokemon("Pikachu", "Rata amarilla molestosa que nunca se deja atrapar por el equipo Rocket", 40, 2, "Electrico", "Roca");

    ash->agregarPokemon(pikachu, 25);

    pokemon *bulbasaur = new pokemon("Bulbasaur", "Una rana con una planta en su lomo que se pone violento con unos latigazos de un tallo", 20, 4, "Hoja", "Fuego");

    ash->agregarPokemon(bulbasaur, 1);

    pokemon *raichu = new pokemon("Raichu", "Waren amarillo anaranjado de Sarge, lo que nunca será el pikachu de ash", 80, 10, "Electrico", "Roca");

    ash->agregarPokemon(raichu, 26);

    ash->estadisticas();

    pokemon *tmp = ash->buscarPokemon("Raichu");
    cout << tmp->getNombre() << " --- " << tmp->getDescripcion() << endl;

    tmp = ash->buscarPokemon(1);
    cout << tmp->getNombre() << " --- " << tmp->getDescripcion() << endl;

    tmp->setDescripcion("Rana fea");

    pokemon *tmp2 = ash->buscarPokemon("Bulbasaur");
    cout << tmp2->getNombre() << " --- " << tmp2->getDescripcion() << endl;

    return 0;
}