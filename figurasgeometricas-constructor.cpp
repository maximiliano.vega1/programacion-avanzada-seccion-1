#include <iostream>
#include <cstring>

using namespace std;

/*
 * Cuadrado tiene un lado
 */

class Cuadrado {
private:
    int lado;
public:
    Cuadrado() {
        cout << "Constructor vacío de cuadrado" << endl;
    }
    Cuadrado(int lado) {
        cout << "Constructor completo de cuadrado" << endl;
        this->setLado(lado);
    }
    int area() {
        return this->lado * this->lado;
    }
    int area(int lado) {
        return lado * lado;
    }

    int perimetro() {
        return 4 * lado;
    }

    int getLado() {
        return this->lado;
    }

    void setLado(int lado) {
        this->lado = lado;
    }
};

/*
 * Rectángulo
 * Lado A y Lado B
 */

class Rectangulo {
private:
    int base, altura;
public:
    int getBase() {
        return base;
    }

    int getAltura() {
        return altura;
    }

    void setBase(int base) {
        this->base = base;
    }

    void setAltura(int altura) {
        this->altura = altura;
    }

    int area() {
        return this->base * this->altura;
    }

    int perimetro() {
        return this->base * 2 + this->altura * 2;
    }
};

int main() {
    Cuadrado *x; // 0x1283712983
    x = new Cuadrado(5);

    cout << "Perimetro: " << x->perimetro() << endl;

    cout << "Area: " << x->area() << endl;

    cout << "Area personalizada: " << x->area(10) << endl;

    return 0;
}