#include <iostream>

using namespace std;

class Cuenta {
private:
    int numero;
    string nombretitular;
    int monto;
    Cuenta *next;
public:
    Cuenta() {
        monto = 0;
    }
    Cuenta(int numero, string nombretitular, int monto) {
        this->numero = numero;
        this->nombretitular = nombretitular;
        this->monto = monto;
    }
    int getNumero() {
        return numero;
    }
    string getNombretitular() {
        return nombretitular;
    }
    int getMonto() {
        return monto;
    }
    void setNumero(int numero) {
        this->numero = numero;
    }
    void setMonto(int monto) {
        this->monto = monto;
    }
    void setNombretitular(string nombretitular) {
        this->nombretitular = nombretitular;
    }
    void setNext(Cuenta *next) {
        this->next = next;
    }
    Cuenta *getNext() {
        return next;
    }
};


class Sucursal {
private:
    Cuenta *root;
public:
    Sucursal() {
        root = nullptr;
    }
    Sucursal(Cuenta *root) {
        this->root = root;
    }
    void insertarPila(Cuenta *nueva) {
        if(root == nullptr) {
            root = nueva;
            return;
        }
        nueva->setNext(root);
        root = nueva;
    }
    void insertarCola(Cuenta *nueva) {
        if(root == nullptr) {
            root = nueva;
            return;
        }
        Cuenta *aux = root;
        while(aux->getNext() != nullptr) {
            aux = aux->getNext();
        }
        aux->setNext(nueva);
    }
    void imprimir() {
        Cuenta *aux = root;
        while(aux != nullptr) {
            cout << aux->getNumero() << "->";
            aux = aux->getNext();
        }
        cout << "NULL" << endl;
    }
    void eliminar() {
        Cuenta *eliminar = root;
        root = root->getNext();
        delete eliminar;
    }
    void eliminarBusqueda(int numero) { // 444
        if(root == nullptr) { return; }
        if(root->getNumero() == numero) {
            this->eliminar();
            return;
        }
        if(root->getNext() == nullptr) {
            return;
        }
        Cuenta *aux = root;
        while(aux->getNext()->getNumero() != numero) {
            aux = aux->getNext();
            if(aux == nullptr) {
                return;
            }
        }
        Cuenta *eliminar = aux->getNext();
        aux->setNext(aux->getNext()->getNext());
        delete eliminar;
    }
};

int main() {
    Sucursal *nunoa = new Sucursal();
    Cuenta *profe = new Cuenta(17374943, "Maximiliano Vega", 1000);
    nunoa->insertarPila(profe);
    nunoa->imprimir();
    Cuenta *cesarmunoz = new Cuenta(16451232, "Cesar Munoz", 5000);
    nunoa->insertarPila(cesarmunoz);
    nunoa->imprimir();
    Cuenta *ignaciomarambio = new Cuenta(15322123, "Ignacio Marambio", 10000);
    nunoa->insertarCola(ignaciomarambio);
    nunoa->imprimir();
    nunoa->eliminar();
    nunoa->imprimir();

    return 0;
}