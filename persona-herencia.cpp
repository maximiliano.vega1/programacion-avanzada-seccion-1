#include <iostream>

using namespace std;

class Persona {
private:
    int edad;
    string nombre;
public:
    Persona() {
    }

    Persona(int edad, string nombre) {
        this->edad = edad;
        this->nombre = nombre;
    }

    int getEdad() {
        return this->edad;
    }

    string getNombre() {
        return this->nombre;
    }

    void setEdad(int edad) {
        this->edad = edad;
    }

    void setNombre(string nombre) {
        this->nombre = nombre;
    }

};

class Estudiante : public Persona {
private:
    int rankingEscuela;
    int rankingInstitucion;
    string institucion;
public:
    Estudiante() : Persona() {

    }

    Estudiante(int edad, string nombre, int rankingEscuela, int rankingInstitucion, string institucion)
            : Persona(edad, nombre) {
        this->rankingEscuela = rankingEscuela;
        this->rankingInstitucion = rankingInstitucion;
        this->institucion = institucion;
    }
};

class Ayudante : public Estudiante {
private:
    int sueldo;
    string categoria;
public:
    Ayudante() : Estudiante() {}

    Ayudante(int edad,
             string nombre,
             int rankingEscuela,
             int rankingInstitucion, string institucion,
             int sueldo,
             string categoria)
            :
            Estudiante(
                    edad,
                    nombre,
                    rankingEscuela,
                    rankingInstitucion,
                    institucion
            ) {
        this->sueldo = sueldo;
        this->categoria = categoria;
    }
};


int main() {
}