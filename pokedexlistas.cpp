#include <iostream>

using namespace std;

class Pokemon {
private:
    string tipo;
    string debilidad;
    string nombre;
    int numero;
    Pokemon *next;
public:
    Pokemon() {
        this->next = nullptr;
    }
    Pokemon(string tipo, string debilidad, string nombre, int numero, Pokemon *next) {
        this->nombre = nombre;
        this->tipo = tipo;
        this->debilidad = debilidad;
        this->numero = numero;
        this->next = next;
    }
    string getTipo() {
        return tipo;
    }
    string getDebilidad() {
        return debilidad;
    }
    string getNombre() {
        return nombre;
    }
    int getNumero() {
        return numero;
    }
    Pokemon *getNext() {
        return next;
    }
    void setNext(Pokemon *next) {
        this->next = next;
    }
};
class Pokedex {
private:
    Pokemon *root;
public:
    Pokedex() {
        root = nullptr;
    }
    Pokedex(Pokemon *root) {
        this->root = root;
    }
    bool insertar(Pokemon *nuevo) {
        if(root == nullptr) {
            root = nuevo;
            return true;
        }
        Pokemon *aux = root;
        while(aux->getNext() != nullptr) {
            aux = aux->getNext();
        }
        aux->setNext(nuevo);
        return true;
    }
    void imprimir() {
        if(root == nullptr) { return; }
        Pokemon *aux = root;
        while(aux != nullptr) {
            cout << aux->getTipo() << endl;
            cout << aux->getNombre() << endl;
            cout << aux->getDebilidad() << endl;
            cout << aux->getNumero() << endl;
            aux = aux->getNext();
        }
    }
    int getCuenta() {
        if (root == nullptr) { return 0; }
        int cuenta = 0;
        Pokemon *aux = root;
        while (aux != nullptr) {
            cuenta++;
            aux = aux->getNext();
        }
        return cuenta;
    }
};

int main() {
    Pokedex *ash = new Pokedex();

    Pokemon *bulbasaur = new Pokemon("Hoja", "Fuego", "Bulbasaur", 1, nullptr);
    ash->insertar(bulbasaur);
    Pokemon *pikachu = new Pokemon("Electrico", "Roca", "Pikachu", 25, nullptr);
    ash->insertar(pikachu);
    Pokemon *eevee = new Pokemon("Normal", "Lucha", "Eevee", 133, nullptr);
    ash->insertar(eevee);
    ash->imprimir();
    cout << ash->getCuenta() << endl;
}