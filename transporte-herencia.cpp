#include <iostream>

using namespace std;

class Responsable {
private:
    string nombre;
    int edad;
    int sueldo;
public:
    Responsable() {}

    Responsable(string nombre, int edad, int sueldo) {
        this->nombre = nombre;
        this->edad = edad;
        this->sueldo = sueldo;
    }

    void setNombre(string nombre) {
        this->nombre = nombre;
    }

    void setEdad(int edad) {
        this->edad = edad;
    }

    void setSueldo(int sueldo) {
        this->sueldo = sueldo;
    }

    string getNombre() { return nombre; }

    int getEdad() { return edad; }

    int getSueldo() { return sueldo; }
};

class Conductor : public Responsable {
public:
    Conductor() : Responsable() {}

    Conductor(string nombre, int edad, int sueldo) : Responsable(nombre, edad, sueldo) {}
};

class Piloto : public Responsable {
public:
    Piloto() : Responsable() {}

    Piloto(string nombre, int edad, int sueldo) : Responsable(nombre, edad, sueldo) {}
};

class Capitan : public Responsable {
public:
    Capitan() : Responsable() {}

    Capitan(string nombre, int edad, int sueldo) : Responsable(nombre, edad, sueldo) {}
};

class Transporte {
private:
    string marca;
    string modelo;
    string estado;
    string color;
public:
    Transporte() {}

    Transporte(string marca, string modelo, string estado, string color) {
        this->marca = marca;
        this->modelo = modelo;
        this->estado = estado;
        this->color = color;
    }

    void setModelo(string modelo) {
        this->modelo = modelo;
    }

    void setMarca(string marca) {
        this->marca = marca;
    }

    void setColor(string color) {
        this->color = color;
    }

    void setEstado(string estado) {
        this->estado = estado;
    }

    string getModelo() {
        return modelo;
    }

    string getMarca() {
        return marca;
    }

    string getColor() {
        return color;
    }

    string getEstado() {
        return estado;
    }
};

class UltimaMilla : public Transporte {
private:
    float capacidadVol;
    int asientos;
    int anio;
    string tipoLicencia;
public:
    UltimaMilla() : Transporte() {}

    UltimaMilla(string marca, string modelo, string color, string estado,
                float capacidadVol, int asientos, int anio, string tipoLicencia) :
            Transporte(marca, modelo, estado, color) {
        this->capacidadVol = capacidadVol;
        this->asientos = asientos;
        this->anio = anio;
        this->tipoLicencia = tipoLicencia;
    }

    float getCapacidadVol() {
        return capacidadVol;
    }

    int getAsientos() {
        return asientos;
    }

    int getAnio() {
        return anio;
    }

    string getTipoLicencia() {
        return tipoLicencia;
    }

    void setCapacidadVol(float capacidadVol) {
        this->capacidadVol = capacidadVol;
    }

    void setAsientos(int asientos) {
        this->asientos = asientos;
    }

    void setAnio(int anio) {
        this->anio = anio;
    }

    void setTipoLicencia(string tipoLicencia) {
        this->tipoLicencia = tipoLicencia;
    }

};

class Automovil : public UltimaMilla {
private:
    string patente;
    Conductor *conductor;
public:
    Automovil() : UltimaMilla() {
        conductor = nullptr;
    }

    Automovil(string marca, string modelo, string color, string estado, float capacidadVol, int asientos, int anio,
              string tipoLicencia, string patente, Conductor *conductor) :
            UltimaMilla(marca, modelo, color, estado, capacidadVol, asientos, anio, tipoLicencia) {
        this->patente = patente;
        this->conductor = conductor;
    }

    void setPatente(string patente) {
        this->patente = patente;
    }

    string getPatente() {
        return patente;
    }

    void setConductor(Conductor *conductor) {
        this->conductor = conductor;
    }

    Conductor *getConductor() {
        return conductor;
    }
};


class Bicicleta : public UltimaMilla {
private:
    float aro;
    Conductor *conductor;
public:
    Bicicleta() : UltimaMilla() {
        conductor = nullptr;
    }

    Bicicleta(string marca, string modelo, string color, string estado, float capacidadVol, int asientos, int anio,
              string tipoLicencia, float aro, conductor *conductor) :
            UltimaMilla(marca, modelo, color, estado, capacidadVol, asientos, anio, tipoLicencia) {
        this->aro = aro;
        this->conductor = conductor;
    }

    void setAro(float aro) {
        this->aro = aro;
    }

    float getAro() {
        return aro;
    }

    void setConductor(Conductor *conductor) {
        this->conductor = conductor;
    }

    Conductor *getConductor() {
        return conductor;
    }
};

class Masivo : public Transporte {
private:
    float volumenMax;
    string paisOrigen;
    int escalas;
public:
    Masivo() : Transporte() {}

    Masivo(float volumenMax, string paisorigen, int escalas, string marca, string modelo, string estado, string color)
            : Transporte(marca, modelo, estado, color) {
        this->volumenMax = volumenMax;
        this->paisorigen = paisorigen;
        this->escalas = escalas;
    }

    void setPaisOrigen(string paisOrigen) {
        this->paisOrigen = paisOrigen;
    }

    void setVolumenMax(float volumenMax) {
        this->volumenMax = volumenMax;
    }

    void setEscalas(int escalas) {
        this->escalas = escalas;
    }

    float getVolumenMax() {
        return this->volumenMax;
    }

    string getPaisOrigen() {
        return this->paisOrigen;
    };

    int getEscalas() {
        return this->escalas;
    }
};

class Avion : public Masivo {
private:
    Piloto *piloto;
public:

    Avion() : Masivo() {
        piloto = nullptr;
    }

    Avion(float volumenMax, string paisorigen, int escalas, string marca, string modelo, string estado, string color,
          Piloto *piloto) :
            Masivo(volumenMax, paisorigen, escalas, marca, modelo, estado, color) { this->piloto = piloto; }

    void setPiloto(Piloto *piloto) {
        this->piloto = piloto;
    }

    Piloto *getPiloto() { return piloto; }
}

class Camion : public UltimaMilla {
private:
    string patente;
    string proveedor;
    Conductor *conductor;
public:
    Camion() : UltimaMilla() {
        conductor = nullptr;
    }

    Camion(string marca, string modelo, string color, string estado,
           float capacidadVol, int asientos, int anio, string tipoLicencia,
           string patente, string proveedor, Conductor *conductor) : UltimaMilla(marca, modelo, color,
                                                                                 estado, capacidadVol, asientos, anio,
                                                                                 tipoLicencia) {
        this->patente = patente;
        this->proveedor = proveedor;
        this->conductor = conductor;
    }

    void setConductor(Conductor *conductor) {
        this->conductor = conductor;
    }

    Conductor *getConductor() { return conductor; }

    string getPatente() {
        return patente;
    }

    string getProveedor() {
        return proveedor;
    }

    void setPatente(string patente) {
        this->patente = patente;
    }

    void setProveedor(string proveedor) {
        this->proveedor = proveedor;
    }
};

class Barco : public Masivo {
private:
    Capitan *capitan;
public:
    Barco() : Masivo() {
        capitan = nullptr;
    }

    Barco(float volumenMax, string paisorigen, int escalas, string marca, string modelo,
          string estado, string color, Capitan *capitan) : Masivo(volumenMax, paisorigen, escalas, marca,
                                                                  modelo, estado, color) {
        this->capitan = capitan;
    }


    void setCapitan(Capitan *capitan) {
        this->capitan = capitan;
    }

    Capitan *getCapitan() { return capitan; }

};


