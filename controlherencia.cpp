class Empleado {
private:
    string nombre;
public:
    void setNombre(string nombre) {
        this->nombre = nombre;
    }
    Empleado(string nombre) {
        this->setNombre(nombre);
    }
    string getNombre() {
        return nombre;
    }
};
class Personal : public Empleado {
private:
    string cargo;
public:
    void setCargo(string cargo) {
        this->cargo = cargo;
    }
    Personal(string nombre, string cargo):Empleado(nombre) {
        this->setCargo(cargo);
    }
    string getCargo() {
        return cargo;
    }
};
class Profesorado : public Empleado {
private:
    string especializacion;
public:
    void setEspecializacion(string especializacion) {
        this->especializacion = especializacion;
    }
    Profesorado(string nombre, string especializacion): Empleado(nombre) {
        this->setEspecializacion(especializacion);
    }
    string getEspecializacion() {
        return especializacion;
    }
};



class Cordinador: public Profesorado {
private:
    Profesor *profesores[10];
public:
    Cordinador(string nombre, string especializacion, Profesor *profesores[]): Profesorado(nombre, especializacion) {
        for(int i = 0; i < 10; i++) {
            this->profesores[i] = profesores[i];
        }
    }
    Cordinador():Profesorado() {
        for(int i = 0; i < 10; i++) {
            this->profesores[i] = nullptr;
        }
    }
};
class Estudiante {
private:
    int anioDeIngreso;
    int ranking;
    float promedioAcumulado;
public:
    Estudiante() {}
};
class Ayudante : public Empleado, public Estudiante {

};


class Profesor: public Profesorado {
private:
    Estudiante *estudiantes[150];
    Ayudante *ayudantes[10];
public:
    Profesor(string nombre, string especializacion, Estudiante *estudiantes[], Ayudante *ayudantes[]): Profesorado(nombre, especializacion) {
        for(int i = 0; i < 150; i++) {
            this->estudiantes[i] = estudiantes[i];
        }
        for(int i = 0; i < 10; i++) {
            this->ayudantes[i] = ayudantes[i];
        }
    }
    Profesor():Profesorado() {
        for(int i = 0; i < 150; i++) {
            this->estudiantes[i] = nullptr;
        }
        for(int i = 0; i < 10; i++) {
            this->ayudantes[i] = nullptr;
        }
    }
};