#include <iostream>
//
// Created by Maximiliano Vega on 24-09-20.
//

using namespace std;

class CuentaBase {
private:
    int numero;
    string nombre;
    int monto;
    int limiteMonto;
    int limiteGiro;
    int limiteDeposito;
public:
    CuentaBase() {
        monto = 0;
    }

    CuentaBase(int numero, string nombre, int monto) {
        this->numero = numero;
        this->nombre = nombre;
        this->monto = monto;
    }

    int getMonto() { return monto; }

    string getNombre() { return nombre; }

    int getNumero() { return numero; }

    void setMonto(int monto) { this->monto = monto; }

    void setNombre(string nombre) { this->nombre = nombre; }

    void setNumero(int numero) { this->numero = numero; }

    void setLimiteMonto(int limiteMonto) { this->limiteMonto = limiteMonto; }

    int getLimiteMonto() { return this->limiteMonto; }

    void setLimiteGiro(int limiteGiro) { this->limiteGiro = limiteGiro; }

    int getLimiteGiro() { return this->limiteGiro; }

    void setLimiteDeposito(int limiteDeposito) { this->limiteDeposito = limiteDeposito; }

    int getLimiteDeposito() { return this->limiteDeposito; }

    void girar(int monto) {
        if (monto > this->getMonto() || monto > this->getLimiteGiro()) {
            cout << "No es posible realizar el giro, revise su saldo o bien, no puede girar más de "
                 << this->getLimiteGiro() << endl;
            return;
        }
        this->setMonto(this->getMonto() - monto);
        cout << "Monto girado con exito, nuevo saldo: " << this->getMonto() << endl;
    }
};

// Cuenta vista tiene un limite de 3000000 en monto
// Limite de giro de 200.000 diarios
// Limite de deposito de 1.000.000
class CuentaVista : public CuentaBase {
public:
    void init() {
        this->setLimiteMonto(3000000);
        this->setLimiteGiro(200000);
        this->setLimiteDeposito(1000000);
    }

    CuentaVista(int numero, string nombre, int monto) : CuentaBase(numero, nombre, monto) { init(); }

    CuentaVista() : CuentaBase() { init(); }
};

// Cuenta cuenta de ahorro tiene un limite de 20000000 en monto
// Limite de giro de 200.000 diarios
// Limite de giros por mes es 1
// Limite de deposito de 20.000.000
class CuentaAhorro : public CuentaBase {
private:
    int girosRealizados;
public:
    void init() {
        this->setLimiteMonto(20000000);
        this->setLimiteGiro(200000);
        this->setLimiteDeposito(20000000);
        girosRealizados = 0;
    }

    CuentaAhorro(int numero, string nombre, int monto) : CuentaBase(numero, nombre, monto) { init(); }

    CuentaAhorro() : CuentaBase() { init(); }

    void girar(int monto) {
        if (monto > this->getMonto() || monto > this->getLimiteGiro() || girosRealizados == 0) {
            cout << "No es posible realizar el giro, revise su saldo o bien, no puede girar más de "
                 << this->getLimiteGiro() << " o también, excedio su cantidad de giros del mes" << endl;
            return;
        }
        this->setMonto(this->getMonto() - monto);
        cout << "Monto girado con exito, nuevo saldo: " << this->getMonto() << endl;
        this->girosRealizados++;
    }
};

// Cuenta cuenta de corriente tiene un limite de 100000000 en monto
// Limite de giro de 400.000 diarios
// Limite de deposito de 20.000.000
// Linea de credito 1.000.000
class CuentaCorriente : public CuentaBase {
private:
    int lineaCredito;
public:
    int getLineaCredito() { return lineaCredito; }

    void setLineaCredito(int lineaCredito) {
        this->lineaCredito = lineaCredito;
    }

    void init() {
        this->setLimiteMonto(100000000);
        this->setLimiteGiro(400000);
        this->setLimiteDeposito(20000000);
        this->setLineaCredito(1000000);
    }

    CuentaAhorro(int numero, string nombre, int monto) : CuentaBase(numero, nombre, monto) { init(); }

    CuentaAhorro() : CuentaBase() { init(); }

    void girar(int monto) {
        if (monto > this->getMonto() + this->getLineaCredito()) {
            cout << "No es posible realizar el giro, revise su saldo y su linea de crédito o bien, no puede girar más de "
                 << this->getLimiteGiro() << endl;
            return;
        }
        if(monto > this->getMonto()) {
            // 400.000
            // Saldo: 300.000  -> 0
            // LC: 1.000.000   -> 900.000

            this->setLineaCredito(this->getLineaCredito() - (monto - this->getMonto()));
            this->setMonto(0);
        } else {
            this->setMonto(this->getMonto() - monto);
        }
        cout << "Monto girado con exito, nuevo saldo: " << this->getMonto() << endl;
    }
};


int main() {
    CuentaVista *CV = new CuentaVista();
    cout << CV->getNombre() << endl;
}