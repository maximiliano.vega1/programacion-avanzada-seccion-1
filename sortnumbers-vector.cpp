#include <iostream>
#include <vector>

using namespace std;


class Alumno {
private:
    string nombre;
    int ranking;
    int anioIngreso;
public:
    Alumno() {}

    Alumno(string nombre, int ranking, int anioIngreso) {
        this->nombre = nombre;
        this->ranking = ranking;
        this->anioIngreso = anioIngreso;
    }

    int getRanking() {
        return ranking;
    }

    string getNombre() {
        return nombre;
    }

    int getAnioIngreso() {
        return anioIngreso;
    }

};


bool descendente(Alumno a, Alumno b) {
    int scoreA = a.getRanking() * 0.3 + (2020 - a.getAnioIngreso()) * 0.7;
    int scoreB = b.getRanking() * 0.3 + (2020 - b.getAnioIngreso()) * 0.7;
    return scoreA > scoreB;
}

int main() {
    vector<Alumno> escuelaEIT;
    escuelaEIT.push_back(*new Alumno("Maximiliano Vega", 80, 2009));
    escuelaEIT.push_back(*new Alumno("Cesar Montero", 100, 2020));
    escuelaEIT.push_back(*new Alumno("Matias Miranda", 120, 2020));


    sort(escuelaEIT.begin(), escuelaEIT.end(), descendente);

    for (vector<Alumno>::iterator iter = escuelaEIT.begin(); iter != escuelaEIT.end(); iter++) {
        cout << iter->getNombre() << ": " << iter->getRanking() << " - ";
    }
    cout << endl;


    return 0;
}