#include <iostream>
#include <stack>

using namespace std;

int main() {
    stack<char> palindromo;
    string input;
    char c;
    cout << "Ingrese letra o digito, ingrese 0 para terminar" << endl;
    while (true) {
        c = getchar();
        if (c == '\n') { break; }
        palindromo.push(c);
        input += c;
    }
    string output;
    while (!palindromo.empty()) {
        char l = palindromo.top();
        output += l;
        palindromo.pop();
    }
    cout << input;
    if (input == output) {
       cout << " es un palindromo" << endl;
    } else {
        cout << " no es un palindromo :(" << endl;
    }

    return 0;
}