#include <iostream>
#include <vector>

using namespace std;

class Producto {
private:
    string nombre;
    int precio;
public:
    Producto(string nombre, int precio) {
        this->nombre = nombre;
        this->precio = precio;
    }

    string getNombre() {
        return nombre;
    }

    int getPrecio() {
        return precio;
    }
};

class Cliente {
private:
    vector<Producto> listaDeCompras;
    string rut;
public:
    Cliente(string rut) {
        this->rut = rut;
    }

    void agregarProducto(string nombre, int precio) {
        this->listaDeCompras.push_back(*new Producto(nombre, precio));
    }

    string getRut() {
        return rut;
    }

    Producto obtenerProducto() {
        Producto r = listaDeCompras[0];
        listaDeCompras.erase(listaDeCompras.begin());
        return r;
    }

    bool hayProductos() {
        return !listaDeCompras.empty();
    }

    int cantidadDeProductos() {
        return listaDeCompras.size();
    }
};

class Caja {
private:
    vector<Cliente> cola;
    int ventatotal;
    int clientesatendidos;
public:
    Caja() {
        ventatotal = 0;
        clientesatendidos = 0;
    }

    void encolar(Cliente *nuevo) {
        this->cola.push_back(*nuevo);
    }

    void pagar() {
        if (cola.empty()) {
            cout << "No hay clientes en la caja" << endl;
            return;
        }
        Cliente pagador = this->cola[0];
        cout << pagador.getRut() << endl;
        int sumaProductos = 0;
        cout << "Cantidad de productos: " << pagador.cantidadDeProductos() << endl;
        while (pagador.hayProductos()) {
            Producto r = pagador.obtenerProducto();
            cout << "BIP! " << r.getNombre() << " : " << r.getPrecio() << endl;
            sumaProductos += r.getPrecio();
        }
        cout << "Total a pagar: " << sumaProductos << endl;
        ventatotal += sumaProductos;
        clientesatendidos++;
        this->cola.erase(cola.begin());
    }

    int cantidadClientes() {
        return cola.size();
    }

    int getVentaTotal() {
        return ventatotal;
    }

    int getClientesAtendidos() {
        return clientesatendidos;
    }
};


int main() {
    vector<Caja> cajas;
    cajas.push_back(*new Caja());
    int opcion;

    int ultimacaja = 0;

    while (true) {
        cout << "Bienvenidos al supermercado inflacion covid19" << endl
             << "1. Agregar cliente" << endl
             << "2. Procesar cliente" << endl
             << "3. Agregar caja" << endl
             << "4. Cerrar local" << endl
             << "0. Salir" << endl;
        cin >> opcion;
        if (opcion == 0) {
            break;
        } else if (opcion == 1) {
            string rut;
            cout << "Cual es el RUT del cliente?" << endl;
            cin >> rut;
            Cliente *nuevo = new Cliente(rut);
            string producto;
            int precio;
            while (true) {
                cin.ignore();
                cout << "Que producto desea agregar, ingrese 0 para finalizar" << endl;
                getline(cin, producto);
                if (producto == "0") {
                    break;
                }
                cout << "Que precio tiene ese producto" << endl;
                cin >> precio;
                nuevo->agregarProducto(producto, precio);
            }
            cajas[ultimacaja].encolar(nuevo);
            ultimacaja++;
            if (ultimacaja > cajas.size() - 1) {
                ultimacaja = 0;
            }
        } else if (opcion == 2) {
            for (int i = 0; i < cajas.size(); i++) {
                cout << "Caja " << i + 1 << ": " << cajas[i].cantidadClientes() << " clientes en cola" << endl;
            }
            cout << "En que caja desea procesar?" << endl;
            cin >> opcion;
            cajas[opcion - 1].pagar();
        } else if (opcion == 3) {
            cout << "Cajas actuales: " << cajas.size() << endl;
            cajas.push_back(*new Caja());
            cout << "Nueva cantidad de cajas: " << cajas.size() << endl;
        } else if (opcion == 4) {
            int totalVentaLocal = 0;
            int totalClientesLocal = 0;
            for (int i = 0; i < cajas.size(); i++) {
                cout << "Caja " << i + 1 << " - Venta total: " << cajas[i].getVentaTotal() << endl;
                cout << "Caja " << i + 1 << " - Clientes atendidos: " << cajas[i].getClientesAtendidos() << endl;
                totalClientesLocal += cajas[i].getClientesAtendidos();
                totalVentaLocal += cajas[i].getVentaTotal();
            }
            cout << "Total por local" << endl
                 << "Clientes atendidos: " << totalClientesLocal << endl
                 << "Venta local: " << totalVentaLocal << endl;
        } else {
            cout << "Opcion invalida :(" << endl;
        }
    }
    return 0;
}
