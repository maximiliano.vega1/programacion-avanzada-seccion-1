#include <iostream>
#include <cstring>

using namespace std;

void pause() {
    cout << "Ingrese cualquier tecla para continuar" << endl;
    getchar();
}

/*
 * Cuenta de banco: esta cuenta debe permitir realizar giros (sacar dinero), abonar(depositar dinero)
 * obtener saldo, obtener los datos del cliente
 */

class Cuenta {
private:
    int numero;
    int saldo;
public:
    Cuenta() {
        this->saldo = 0;
    }

    Cuenta(int numero) {
        this->saldo = 0;
        this->numero = numero;
    }

    int getSaldo() {
        return this->saldo;
    }

    int getNumero() {
        return this->numero;
    }

    void setSaldo(int saldo) {
        this->saldo = saldo;
    }

    void setNumero(int numero) {
        this->numero = numero;
    }

    void deposito(int abono) {
        this->saldo += abono;
    }

    bool giro(int cantidad) {
        if (cantidad > this->saldo || this->saldo == 0) {
            return false;
        }
        this->saldo -= cantidad;
        return true;
    }

    bool transferencia(Cuenta *destinatario, int monto) {
        if (monto > this->saldo) {
            return false;
        }
        destinatario->deposito(monto);
        this->saldo -= monto;
        return true;
    }
};

class Cliente {
private:
    string correo;
    string rut;
    string nombre;
    Cuenta *cuentas[5];
    Cuenta *preferida;
public:
    Cliente() {
        correo = "";
        rut = "";
        nombre = "";
        for (int i = 0; i < 5; i++) {
            cuentas[i] = nullptr;
        }
        preferida = nullptr;
    }

    Cliente(string correo, string rut, string nombre) {
        Cliente();
        this->setCorreo(correo);
        this->setRut(rut);
        this->setNombre(nombre);
    }

    string getCorreo() {
        return this->correo;
    }

    string getRut() {
        return this->rut;
    }

    string getNombre() {
        return this->nombre;
    }

    void setCorreo(string correo) {
        this->correo = correo;
    }

    void setRut(string rut) {
        this->rut = rut;
    }

    void setNombre(string nombre) {
        this->nombre = nombre;
    }

    Cuenta *getCuenta(int indice) {
        return this->cuentas[indice];
    }

    void setCuenta(Cuenta *cuenta, int indice) {
        this->cuentas[indice] = cuenta;
    }

    bool agregarCuenta(Cuenta *cuenta) {
        for (int i = 0; i < 5; i++) {
            if (this->cuentas[i] == nullptr) {
                this->cuentas[i] = cuenta;
                return true;
            }
        }
        return false;
    }

    bool agregarCuenta(int numero) {
        for (int i = 0; i < 5; i++) {
            if (this->cuentas[i] == nullptr) {
                this->cuentas[i] = new Cuenta(numero);
                return true;
            }
        }
        return false;
    }

    Cuenta *getPreferida() {
        return this->preferida;
    }

    void setPreferida(Cuenta *preferida) {
        this->preferida = preferida;
    }
};

int main() {
    int opcion;
    string ingreso;

    Cliente *clientes[3];
    int cantidadClientes = 0;

    while (true) {
        cout << "Administrador de cuentas del banco UDP" << endl
             << "1. Crear cliente" << endl
             << "2. Crear cuenta a cliente" << endl
             << "Ingrese Opcion: " << endl;

        cin >> opcion;

        if (opcion == 1) {
            if (cantidadClientes == 3) {
                cout << "Ya ha llegado a la cantidad máxima de clientes" << endl;
                continue;
            }

            Cliente *nuevo = new Cliente();

            cin.ignore();

            cout << "Ingrese correo" << endl;
            getline(cin, ingreso);
            nuevo->setCorreo(ingreso);

            cout << "Ingrese nombre" << endl;
            getline(cin, ingreso);
            nuevo->setNombre(ingreso);

            cout << "Ingrese RUT" << endl;
            getline(cin, ingreso);
            nuevo->setRut(ingreso);

            clientes[cantidadClientes] = nuevo;
            cantidadClientes++;

            cout << "Cliente creado con éxito :)" << endl;
        } else if (opcion == 2) {
            cout << "Ingrese el numero de cuenta" << endl;
            cin >> opcion;
            Cuenta *nueva = new Cuenta(opcion);

            for (int i = 0; i < cantidadClientes; i++) {
                cout << "[ID: " << i+1 << "] "
                     << "[Nombre: " << clientes[i]->getNombre()
                     << "] [RUT: " << clientes[i]->getRut()
                     << "] [Correo: " << clientes[i]->getCorreo() << "]"
                     << endl;
            }
            cout << "Ingrese el ID del cliente para ingresar la cuenta" << endl;
            cin >> opcion;

            if(clientes[opcion-1]->agregarCuenta(nueva)) {
                cout << "Cuenta agregada con éxito" << endl;
            } else {
                cout << "No se pudo agregar la cuenta, el cliente ya cuenta con 5 cuentas" << endl;
            }
        }
        pause();
    }

    return 0;
}