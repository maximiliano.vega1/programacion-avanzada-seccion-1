#include <iostream>

using namespace std;

/*
 * Cuadrado tiene un lado
 */

class Cuadrado {
private:
    int lado;
public:
    int area() {
        return lado * lado;
    }

    int perimetro() {
        return 4 * lado;
    }

    int getLado() {
        return this->lado;
    }

    void setLado(int lado) {
        this->lado = lado;
    }
};

/*
 * Rectángulo
 * Lado A y Lado B
 */

class Rectangulo {
private:
    int base, altura;
public:
    int getBase() {
        return base;
    }

    int getAltura() {
        return altura;
    }

    void setBase(int base) {
        this->base = base;
    }

    void setAltura(int altura) {
        this->altura = altura;
    }

    int area() {
        return this->base * this->altura;
    }

    int perimetro() {
        return this->base * 2 + this->altura * 2;
    }
};

int main() {
    Cuadrado *x = new Cuadrado();
    Cuadrado *y = new Cuadrado();

    x->setLado(5);

    cout << x->getLado() << endl;

    cout << "Lado: " << x->getLado() << endl;
    cout << "Area: " << x->area() << endl;
    cout << "Perímetro: " << x->perimetro() << endl;

    int valor;

    cout << "Por favor, ingrese el valor del lado" << endl;
    cin >> valor;
    y->setLado(valor);
    cout << "Lado: " << y->getLado() << endl;
    cout << "Area: " << y->area() << endl;
    cout << "Perímetro: " << y->perimetro() << endl;

    cout << "RECTÁNGULO AHORA :D" << endl;

    Rectangulo *HolaSoyUnRectangulo;  // --> 0x38417293
    HolaSoyUnRectangulo = new Rectangulo();

    HolaSoyUnRectangulo->setAltura(10);
    HolaSoyUnRectangulo->setBase(5);
    cout << "Altura: " << HolaSoyUnRectangulo->getAltura() << " - Base: " << HolaSoyUnRectangulo->getBase() << endl;
    cout << "Area: " << HolaSoyUnRectangulo->area() << endl;
    cout << "Perímetro: " << HolaSoyUnRectangulo->perimetro() << endl;
}