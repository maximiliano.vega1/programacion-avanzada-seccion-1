#include <iostream>

using namespace std;

class Empleado {
private:
    string nombre;
public:
    Empleado() {
    }

    Empleado(string nombre) {
        this->nombre = nombre;
    }

    string getNombre() {
        return nombre;
    }

    void setNombre(string nombre) {
        this->nombre = nombre;
    }
};

class EmpleadoHoras : public Empleado {
private:
    int horas;
    string horarioInicio;
    string horarioTermino;
public:
    EmpleadoHoras() : Empleado() {}

    EmpleadoHoras(string nombre, int horas, string horarioInicio, string horarioTermino) : Empleado(nombre) {
        this->horas = horas;
        this->horarioInicio = horarioInicio;
        this->horarioTermino = horarioTermino;
    }

    int getHoras() {
        return horas;
    }

    string getHorarioInicio() {
        return horarioInicio;
    }

    string getHorarioTermino() {
        return horarioTermino;
    }

    void setHorarioInicio(string horarioInicio) {
        this->horarioInicio = horarioInicio;
    }

    void setHorarioTermino(string horarioTermino) {
        this->horarioTermino = horarioTermino;
    }

    void setHoras(int horas) {
        this->horas = horas;
    }
};

class Vendedor : public EmpleadoHoras {
private:
    int comision;
    int ventas;
public:
    Vendedor() : EmpleadoHoras() {}

    Vendedor(string nombre, int horas, string horarioInicio, string horarioTermino, int comision, int ventas) :
            EmpleadoHoras(nombre, horas, horarioInicio, horarioTermino) {
        this->comision = comision;
        this->ventas = ventas;
    }

    void setComision(int comision) {
        this->comision = comision;
    }

    void setVentas(int ventas) {
        this->ventas = ventas;
    }

    int getComision() {
        return comision;
    }

    int getVentas() {
        return ventas;
    }
};

class Gerente : public Empleado {
private:
    int sueldoSemanal;
public:
    Gerente() : Empleado() {}

    Gerente(string nombre, int sueldoSemanal) : Empleado(nombre) {
        this->sueldoSemanal = sueldoSemanal;
    }

    void setSueldoSemanal(int sueldoSemanal) {
        this->sueldoSemanal = sueldoSemanal;
    }

    int getSueldoSemanal() {
        return sueldoSemanal;
    }
};

class GerenteVenta : public Gerente, public Vendedor {
public:
    GerenteVenta() : Gerente(), Vendedor() {}

    GerenteVenta(string nombre, int sueldoSemanal, int horas, string horarioInicio,
                 string horarioTermino, int comision, int ventas) : Gerente(nombre, sueldoSemanal),
                                                                    Vendedor(nombre, horas, horarioInicio,
                                                                             horarioTermino, comision, ventas) {}

    void setNombre(string nombre) {
        this->Vendedor::setNombre(nombre);
        this->Gerente::setNombre(nombre);
    }
    string getNombre() {
        return this->Vendedor::getNombre();
    }
};

int main() {
    Empleado *e = new Empleado();
    e->setNombre("Lolo");

    Gerente *g = (Gerente*) e;
    g->setSueldoSemanal(10000);
    cout<< g->getSueldoSemanal() << endl;
    cout<< g->getNombre() << endl;
}