#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<char> frase;
    char c;
    cout << "Ingrese letra o digito, ingrese 0 para terminar" << endl;
    while (true) {
        c = getchar();
        if (c == '\n') { continue; }
        if (c == '0') { break; }
        frase.insert(frase.begin(), c);
        //frase.push_back(c);
    }
    cout << "Frase al reves: " << endl;
    while(!frase.empty()) {
        cout << frase[0];
        frase.erase(frase.begin());
    }

    cout << endl;
    return 0;
}