#include <iostream>
#include <vector>

using namespace std;

class Cuenta {
private:
    int numero;
    string nombretitular;
    int monto;
public:
    Cuenta() {
        monto = 0;
    }
    Cuenta(int numero, string nombretitular, int monto) {
        this->numero = numero;
        this->nombretitular = nombretitular;
        this->monto = monto;
    }
    int getNumero() {
        return numero;
    }
    string getNombretitular() {
        return nombretitular;
    }
    int getMonto() {
        return monto;
    }
    void setNumero(int numero) {
        this->numero = numero;
    }
    void setMonto(int monto) {
        this->monto = monto;
    }
    void setNombretitular(string nombretitular) {
        this->nombretitular = nombretitular;
    }
};


int main() {
    vector<Cuenta> nunoa;
    nunoa.push_back(*new Cuenta(123, "max", 2000));
    cout << (nunoa.at(0)).getNombretitular() << endl;
}