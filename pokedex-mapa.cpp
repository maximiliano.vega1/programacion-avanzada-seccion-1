#include <iostream>
#include <map>
#include <vector>
#include <cstring>

using namespace std;

class Pokemon {
private:
    string nombre;
    string descripcion;
    int altura;
    int peso;
    string tipo;
    string debilidad;
    map<string, int> habilidades;
public:
    Pokemon() {}

    Pokemon(string nombre,
            string descripcion,
            int altura,
            int peso,
            string tipo,
            string debilidad
    ) {
        this->nombre = nombre;
        this->descripcion = descripcion;
        this->altura = altura;
        this->peso = peso;
        this->tipo = tipo;
        this->debilidad = debilidad;
    }

    string getNombre() {
        return nombre;
    }

    string getDescripcion() {
        return descripcion;
    }

    string getTipo() {
        return tipo;
    }

    string getDebilidad() {
        return debilidad;
    }

    int getAltura() {
        return altura;
    }

    int getPeso() {
        return peso;
    }

    map<string, int> getHabilidades() {
        return this->habilidades;
    }

    void setNombre(string nombre) {
        this->nombre = nombre;
    }

    void setDescripcion(string descripcion) {
        this->descripcion = descripcion;
    }

    void setTipo(string tipo) {
        this->tipo = tipo;
    }

    void agregarHabilidad(string habilidad, int potencia) {
        this->habilidades[habilidad] = potencia;
    }

    void setDebilidad(string debilidad) {
        this->debilidad = debilidad;
    }

    void setPeso(int peso) {
        this->peso = peso;
    }

    void setAltura(int altura) {
        this->altura = altura;
    }

    void imprimirHabilidades() {
        cout << "Habilidades" << endl;
        for (map<string, int>::iterator it = habilidades.begin(); it != habilidades.end(); it++) {
            cout << "Habilidad: " << it->first << " => Potencia: " << it->second << endl;
        }
    }
};

string getDebilidad(string tipo) {
    if (tipo == "Fuego") {
        return string("Agua");
    } else if (tipo == "Fuego") {
        return string("Eléctrico");
    } else if (tipo == "Eléctrico") {
        return string("Roca");
    } else if (tipo == "Roca") {
        return string("Agua");
    } else if (tipo == "Tierra") {
        return string("Agua");
    } else if (tipo == "Planta") {
        return string("Fuego");
    } else if (tipo == "Bicho") {
        return string("Volador");
    } else if (tipo == "Normal") {
        return string("Normal");
    } else if (tipo == "Psiquico") {
        return string("Fantasma");
    } else if (tipo == "Lucha") {
        return string("Psiquico");
    } else if (tipo == "Fantasma") {
        return string("Psiquico");
    } else if (tipo == "Volador") {
        return string("Eléctrico");
    } else if (tipo == "Hielo") {
        return string("Fuego");
    } else if (tipo == "Veneno") {
        return string("Roca");
    }
    return string("TIPO INEXISTENTE");
}

string ingresoTipo() {
    int opcion;
    cout << "Tipo del pokémon: " << endl
         << "1. Fuego" << endl
         << "2. Agua" << endl
         << "3. Eléctrico" << endl
         << "4. Roca" << endl
         << "5. Tierra" << endl
         << "6. Planta" << endl
         << "7. Bicho" << endl
         << "8. Normal" << endl
         << "9. Psíquico" << endl
         << "10. Lucha" << endl
         << "11. Fantasma" << endl
         << "12. Volador" << endl
         << "13. Hielo" << endl
         << "14. Veneno" << endl;
    while (true) {
        cin >> opcion;
        if (opcion == 1) {
            return string("Fuego");
        } else if (opcion == 2) {
            return string("Agua");
        } else if (opcion == 3) {
            return string("Eléctrico");
        } else if (opcion == 4) {
            return string("Roca");
        } else if (opcion == 5) {
            return string("Tierra");
        } else if (opcion == 6) {
            return string("Planta");
        } else if (opcion == 7) {
            return string("Bicho");
        } else if (opcion == 8) {
            return string("Normal");
        } else if (opcion == 9) {
            return string("Psiquico");
        } else if (opcion == 10) {
            return string("Lucha");
        } else if (opcion == 11) {
            return string("Fantasma");
        } else if (opcion == 12) {
            return string("Volador");
        } else if (opcion == 13) {
            return string("Hielo");
        } else if (opcion == 14) {
            return string("Veneno");
        } else {
            cout << "Opcion inválida" << endl;
            continue;
        }
    }
}

void imprimirPokemon(Pokemon P) {
    cout << "Nombre \t\t\t: " << P.getNombre() << endl
         << "Descripcion \t\t: " << P.getDescripcion() << endl
         << "Tipo \t\t\t: " << P.getTipo() << endl
         << "Debilidad \t\t: " << P.getDebilidad() << endl
         << "Altura \t\t\t: " << P.getAltura() << endl
         << "Peso \t\t\t: " << P.getPeso() << endl;

    P.imprimirHabilidades();
}

int main() {
    map<int, Pokemon> pokedex;
    map<string, vector<Pokemon> > pokedextipo;
    map<string, vector<Pokemon> > pokedexdebilidad;

    int opcion;
    string ingreso;
    while (true) {
        cout << "Pokedex :D" << endl
             << "1. Agregar pokemon" << endl
             << "2. Consultar pokemon por numero" << endl
             << "3. Consultar pokemon por tipo" << endl
             << "4. Consultar pokemon por debilidad" << endl
             << "0. Salir" << endl;
        cin >> opcion;
        if (opcion == 1) {
            Pokemon P;
            cin.ignore();
            cout << "Nombre del pokémon" << endl;
            getline(cin, ingreso);
            P.setNombre(ingreso);
            cout << "Descripción del pokémon" << endl;
            getline(cin, ingreso);
            P.setDescripcion(ingreso);
            string tipo = ingresoTipo();
            P.setTipo(tipo);
            P.setDebilidad(getDebilidad(tipo));
            cout << "Ingrese peso" << endl;
            cin >> opcion;
            P.setPeso(opcion);
            cout << "Ingrese altura" << endl;
            cin >> opcion;
            P.setAltura(opcion);
            cout << "Ingrese numero de Pokedex" << endl;
            cin >> opcion;
            for (int i = 0; i < 4; i++) {
                cin.ignore();
                cout << "Ingrese habilidad " << i + 1 << endl;
                getline(cin, ingreso);
                cout << "Ingrese potencia" << endl;
                cin >> opcion;
                P.agregarHabilidad(ingreso, opcion);
            }
            pokedex[opcion] = P;
            if (pokedexdebilidad.find(P.getDebilidad()) == pokedexdebilidad.end()) {
                vector<Pokemon> PD;
                PD.push_back(P);
                pokedexdebilidad[P.getDebilidad()] = PD;
            } else {
                pokedexdebilidad[P.getDebilidad()].push_back(P);
            }
            if (pokedextipo.find(P.getTipo()) == pokedextipo.end()) {
                vector<Pokemon> PD;
                PD.push_back(P);
                pokedextipo[P.getTipo()] = PD;
            } else {
                pokedextipo[P.getTipo()].push_back(P);
            }
        } else if (opcion == 0) {
            break;
        } else if (opcion == 3) {
            string tipo = ingresoTipo();
            if (pokedextipo.find(tipo) == pokedextipo.end()) {
                cout << "El tipo " << tipo << " no está en la pokedex :(" << endl;
                continue;
            }
            for (int i = 0; i < pokedextipo[tipo].size(); i++) {
                imprimirPokemon(pokedextipo[tipo][i]);
            }
        } else if (opcion == 4) {
            string tipo = ingresoTipo();
            if (pokedexdebilidad.find(tipo) == pokedexdebilidad.end()) {
                cout << "La debilidad " << tipo << " no está en la pokedex :(" << endl;
                continue;
            }
            for (int i = 0; i < pokedexdebilidad[tipo].size(); i++) {
                imprimirPokemon(pokedexdebilidad[tipo][i]);
            }
        } else if (opcion == 2) {
            cout << "Ingrese el numero" << endl;
            cin >> opcion;
            if (pokedex.find(opcion) == pokedex.end()) {
                cout << "No existe el numero ingresado" << endl;
                continue;
            }
            imprimirPokemon(pokedex[opcion]);
        } else {
            cout << "Opción inválida" << endl;
        }
    }
    return 0;
}