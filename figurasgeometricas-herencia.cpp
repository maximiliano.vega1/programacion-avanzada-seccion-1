#include <iostream>

using namespace std;

class FiguraGeometrica {
private:
    float arista;
public:
    FiguraGeometrica() {}

    FiguraGeometrica(float arista) {
        this->arista = arista;
    }

    void setArista(float arista) {
        this->arista = arista;
    }

    float getArista() {
        return arista;
    }

    float perimetro() {
        return arista;
    }

    float area() {
        return arista;
    }
};

class Rectangulo : public FiguraGeometrica {
private:
    float arista2;
public:
    Rectangulo() : FiguraGeometrica() {}

    Rectangulo(float arista, float arista2) : FiguraGeometrica(arista) {
        this->arista2 = arista2;
    }

    void setArista2(float arista2) {
        this->arista2 = arista2;
    }

    float getArista2() {
        return arista2;
    }

    float perimetro() {
        return 2 * (this->getArista() + arista2);
    }

    float area() {
        return this->getArista() * arista2;
    }
};

class Circunferencia : public FiguraGeometrica {
public:
    Circunferencia() : FiguraGeometrica() {}

    Circunferencia(float radio) : FiguraGeometrica(radio) {}

    float getRadio() {
        return this->getArista();
    }

    void setRadio(float radio) {
        this->setArista(radio);
    }

    float perimetro() {
        return 2 * 3.1416 * this->getRadio();
    }

    float area() {
        return 3.1416 * this->getRadio() * this->getRadio();
    }

};

class Triangulo : public FiguraGeometrica {
private:
    float base, arista2, altura;
public:
    Triangulo() : FiguraGeometrica() {}

    Triangulo(float arista, float arista2, float base, float altura) : FiguraGeometrica(arista) {
        this->arista2 = arista2;
        this->base = base;
        this->altura = altura;
    }

    void setArista2(float arista2) {
        this->arista2 = arista2;
    }

    float getArista2() {
        return arista2;
    }

    void setBase(float base) {
        this->base = base;
    }

    float getBase() {
        return base;
    }

    void setAltura(float altura) {
        this->altura = altura;
    }

    float getAltura() {
        return altura;
    }

    float area() {
        return base * altura / 2;
    }

    float perimetro() {
        return base+this->getArista()+arista2;
    }

};

class TrianguloIsosceles: public Triangulo {
public:
    TrianguloIsosceles() : Triangulo() {}

    TrianguloIsosceles(float lado, float base, float altura) : Triangulo(lado, lado, base, altura) { }
};

int main() {
    TrianguloIsosceles *X = new TrianguloIsosceles(5, 7, 4);

    cout << X->area() << endl;
}